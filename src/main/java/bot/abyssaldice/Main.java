package bot.abyssaldice;


import bot.abyssaldice.commands.*;
import bot.abyssaldice.data_handling.Loader;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public final class Main {
    private static DiscordApi api;
    public final static char prefix = '%';
    public static Loader loader;

    private final static Command[] commands = {
            new ResourceCommand(List.of("resource", "res"),"Prints a random resource type.", ""),
            new RollCommand(List.of("roll","r"),"Roll dice, i.e " + prefix + "roll 3d20+5", "<roll> [modifier]"),
            new PlantCommand(List.of("plant","plants"),"Prints a random plant.", ""),
            new HarvestCommand(List.of("harvest", "harv", "siphon"),"Calculate harvest yields.", "<resource> [count] [break level] [toughness]"),
            new PrintResourcesCommand(List.of("printresources"),"Prints a list of all resources.", ""),
            new HelpCommand(List.of("help",Character.toString(prefix)),"Show command information.", "[command]"),
            new EtherCommand(List.of("ether","element","vis"),"Prints a random element.", ""),
            new CharacterCommand(List.of("character", "char"), "Create, show, or modify a character (WIP).", """
                    <create/edit/show/delete>
                    create : <con> <str> <dex> <int> <will> <inv> <cha> <arc> [description]
                    edit : <attribute> <amount>
                    edit : <con> <str> <dex> <int> <will> <inv> <cha> <arc>
                    desc : <description>
                    delete
                    show: [user id]
                    """),
            new ModifierCommand(List.of("modifiers","globals"), "Update or show global attribute bonuses. Modify is DM-only.", "<con> <str> <dex> <int> <will> <inv> <cha> <arc>")
    };

    public static void main(String[] args) throws IOException { //It all starts here.
        Scanner in = new Scanner(new File("token"));
        api = new DiscordApiBuilder().setToken(in.next()).login().join(); //Login.

        loader = Loader.loadPlayerCharacters(new File("playerdata/", "players.json")); //Load player and globals data.

        //Console listener
        ConsoleInterpreter.getInstance(api);


    // Print the invite url of your bot
        System.out.println("You can invite the bot by using the following url: " + api.createBotInvite());

        api.addMessageCreateListener(event -> {
            String content = event.getMessageContent();
            if(!content.startsWith(Character.toString(prefix))) {
                return;
            }
            if(!event.getMessageAuthor().isRegularUser()) { //No bots.
                event.getChannel().sendMessage("Real accounts only!");
                event.deleteMessage();
                return;
            }
            int commandIndex = identifyCommandIndex(content);
            if (commandIndex != -1) {
                commands[commandIndex].action(event);
            } else {
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + content + "`! I have no idea what this means~!");
            }
        });
    }

    public static Command identifyCommand(String s) { //Identifies what class of command is to be triggered.
        if(s.startsWith(Character.toString(prefix))) {
            s = s.substring(1);
            for (Command command : commands) {
                for (String alias : command.aliases) {
                    if (s.startsWith(alias)) {
                        return command;
                    }
                }
            }
        }
        return null;
    }

    public static int identifyCommandIndex(String s) { //Identifies the command's index to be triggered.
        if(s.startsWith(Character.toString(prefix))) {
            s = s.substring(1);
            int i = 0;
            for (Command command : commands) {
                for (String alias : command.aliases) {
                    if (s.startsWith(alias)) {
                        return i;
                    }
                }
                i++;
            }
        }
        return -1;
    }

    public static Command[] getCommands() {return commands;}

    public static void savePlayerCharacters () {
        loader.savePlayerCharacters(new File("playerdata/", "players.json"));
    }

    public static Loader getPlayerCharacters() {return loader;}

    public static DiscordApi getApi() {return api;}
}
