package bot.abyssaldice;

import bot.abyssaldice.materials.Plant;
import bot.abyssaldice.materials.Resources;

import java.util.Random;

public final class Helper {
    public final static Random rand = new Random();

    public static int[] rollD(String message) {
        message+=" "; //To prevent oob errors.
        boolean diceBeginning = false;
        boolean diceEnd = false;
        int i = 1;
        int count = 1;
        int sides = 2;
        while(!diceBeginning) { //Detect the count of dice.
            if(Character.isDigit(message.charAt(message.indexOf('d') - i))) {
                   i++;
            } else {
                if (i>1) {
                    i--;
                    count = Integer.parseInt(message.substring(message.indexOf('d') - i, message.indexOf('d')));
                }
                diceBeginning = true;
            }
        }
        i = 1;
        while(!diceEnd) { //Detect the sides.
            if(Character.isDigit(message.charAt(message.indexOf('d') + i))) {
                i++;
            } else {
                if(i>1) {
                    sides = Integer.parseInt(message.substring(message.indexOf('d')+1, message.indexOf('d') + i));
                }
                diceEnd = true;
            }
        }


        //Now calculating result.
        int[] results = new int[count+1];
        for(i = 1; i < results.length; i++) {
            results[i]=rand.nextInt(sides)+1;
            results[0]+=results[i];
        }
        return results;
    }

    public static String acquireModifier(String message) {
        message+="  "; //To prevent oob errors.
        boolean diceEnd = false;
        int i = 1;
        String modifier = "";
        while(!diceEnd) { //Detect the sides.
            if(Character.isLetterOrDigit(message.charAt(message.indexOf(' ') + i))) {
                i++;
            } else {
                if(i>1) {
                    modifier = message.substring(message.indexOf(' ') + i);
                    diceEnd = true;
                }
            }
        }
        return modifier.trim();
    }

    public static Plant randomPlant() {
        int etherTypes = 0; //Count of different elements on the plant.
        for(int i = 0; i < 1; i = rand.nextInt(4)) {
            etherTypes++;
        }
        String[] ether = new String[etherTypes];
        for(int i = 0; i < etherTypes; i++) {
            ether[i] = Resources.elements[rand.nextInt(Resources.elements.length)];
            if (ether[i].equals("vol") && Math.random() < 0.5) {
                ether[i] = Resources.elements[rand.nextInt(Resources.elements.length)];
            }
        }
        return new Plant(Plant.types[rand.nextInt(Plant.types.length)], ether);
    }
    public static String formatList(String[] list) {
        String formattedString = "";
        for(int i = 0; i < list.length; i++) {
            if (i + 1 < list.length) {
                formattedString += "`[" + (list[i] + "]`, ");
            } else {
                formattedString += "and `[" + (list[i] + "]`!");
            }
        }
        return formattedString;
    }
}
