package bot.abyssaldice.commands;

import bot.abyssaldice.Helper;
import bot.abyssaldice.Main;
import bot.abyssaldice.materials.Resources;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class PrintResourcesCommand extends Command{

    public PrintResourcesCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + Main.prefix + aliases.get(0) + "`! Here they are: " + Helper.formatList(Resources.getResourceNames()));
        event.deleteMessage();
    }
}
