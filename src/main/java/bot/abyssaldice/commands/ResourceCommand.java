package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import bot.abyssaldice.materials.Resource;
import bot.abyssaldice.materials.Resources;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

import static bot.abyssaldice.Helper.rand;

public class ResourceCommand extends Command{
    public ResourceCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    public static Resource randomResource() {
        return Resources.resources[rand.nextInt(Resources.resources.length)];
    }

    @Override
    public void action(MessageCreateEvent event) {
        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + Main.prefix + aliases.get(0) + "`! It's a wonderful `[" + randomResource().name + "]`!");
        event.deleteMessage();
    }
}
