package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import bot.abyssaldice.materials.Resource;
import bot.abyssaldice.materials.Resources;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

import static bot.abyssaldice.Helper.rand;

public class EtherCommand extends Command{
    public EtherCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    public static String randomElement() {
        return Resources.elements[rand.nextInt(Resources.elements.length)];
    }

    @Override
    public void action(MessageCreateEvent event) { //Random ether type.
        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + Main.prefix + aliases.get(0) + "`! It's `[" + randomElement() + "]`!");
        event.deleteMessage();
    }
}
