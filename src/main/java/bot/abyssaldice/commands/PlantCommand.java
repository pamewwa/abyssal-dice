package bot.abyssaldice.commands;

import bot.abyssaldice.Helper;
import bot.abyssaldice.Main;
import bot.abyssaldice.materials.Plant;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class PlantCommand extends Command{

    public PlantCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {//Random plant.
        Plant coolPlant = Helper.randomPlant();
        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + Main.prefix + aliases.get(0) + "`! It's a lovely [" + coolPlant.type + "] " + coolPlant.writeEther());
        event.deleteMessage();
    }
}
