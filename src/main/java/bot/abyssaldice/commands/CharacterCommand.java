package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import bot.abyssaldice.data_handling.PlayerCharacter;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class CharacterCommand extends Command{

    public CharacterCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        long userId = event.getMessageAuthor().getId();
        String[] parameters = event.getMessageContent().split(" ");
        if(parameters.length>1) {
            if (parameters[1].equalsIgnoreCase("create")) {
                PlayerCharacter player;
                if(parameters.length>9) {
                    player = new PlayerCharacter(userId, Integer.parseInt(parameters[2]), Integer.parseInt(parameters[3]), Integer.parseInt(parameters[4]), Integer.parseInt(parameters[5]), Integer.parseInt(parameters[6]), Integer.parseInt(parameters[7]), Integer.parseInt(parameters[8]), Integer.parseInt(parameters[9]));
                    if (parameters.length>10) {
                        player.desc = event.getMessageContent().substring(event.getMessageContent().indexOf(parameters[10]));
                    }
                } else {
                    player = new PlayerCharacter(userId);
                }
                Main.getPlayerCharacters().players.put(userId, player);
                Main.savePlayerCharacters();
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", done! Use `" + Main.prefix + "char show` to view it!");
                event.deleteMessage();
            } else if (parameters[1].equalsIgnoreCase("edit")) {
                PlayerCharacter player = Main.getPlayerCharacters().players.get(event.getMessageAuthor().getId());
                if (parameters.length==4) {
                    switch (parameters[2]) {
                        case "con" -> player.constitution = Integer.parseInt(parameters[3]);
                        case "str" -> player.strength = Integer.parseInt(parameters[3]);
                        case "dex" -> player.dexterity = Integer.parseInt(parameters[3]);
                        case "int" -> player.intelligence = Integer.parseInt(parameters[3]);
                        case "will" -> player.willpower = Integer.parseInt(parameters[3]);
                        case "inv" -> player.investigation = Integer.parseInt(parameters[3]);
                        case "cha" -> player.charisma = Integer.parseInt(parameters[3]);
                        case "arc" -> player.arcana = Integer.parseInt(parameters[3]);
                    }
                } else {
                    player.constitution=Integer.parseInt(parameters[2]);
                    player.strength=Integer.parseInt(parameters[3]);
                    player.dexterity=Integer.parseInt(parameters[4]);
                    player.intelligence=Integer.parseInt(parameters[5]);
                    player.willpower=Integer.parseInt(parameters[6]);
                    player.investigation=Integer.parseInt(parameters[7]);
                    player.charisma=Integer.parseInt(parameters[8]);
                    player.arcana=Integer.parseInt(parameters[9]);
                }
                Main.getPlayerCharacters().players.put(userId, player);
                Main.savePlayerCharacters();
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", attributes fluffed!");
                event.deleteMessage();
            } else if (parameters[1].equalsIgnoreCase("show")) {
                if(parameters.length==2) {
                    if (Main.getPlayerCharacters().players.containsKey(event.getMessageAuthor().getId())) {
                        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", here you are! \n" + Main.getPlayerCharacters().players.get(event.getMessageAuthor().getId()).format());
                    } else {
                        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", create a character using `" + Main.prefix + "char create`!");
                    }
                } else {
                    if (Main.getPlayerCharacters().players.containsKey(Long.parseLong(parameters[2]))) {
                        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", here they are! \n" + Main.getPlayerCharacters().players.get(Long.parseLong(parameters[2])).format());
                    } else {
                        event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", they don't exist!");
                    }
                }
                event.deleteMessage();
            } else if (parameters[1].equalsIgnoreCase("delete")) {
                Main.getPlayerCharacters().players.remove(event.getMessageAuthor().getId());
                Main.savePlayerCharacters();

                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", I've already forgotten you!");
                event.deleteMessage();
            } else if (parameters[1].equalsIgnoreCase("desc")) {
                if(parameters.length>2) {
                    Main.getPlayerCharacters().players.get(event.getMessageAuthor().getId()).desc = event.getMessageContent().substring(event.getMessageContent().indexOf(parameters[2]));
                    Main.savePlayerCharacters();
                    event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", you are!!");

                    event.deleteMessage();
                } else {
                    event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", who are you?!?");
                    event.deleteMessage();
                }
            }
        } else {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + """
                    , format is
                    ```
                    <create/edit/show/delete>
                    create : <con> <str> <dex> <int> <will> <inv> <cha> <arc> [description]
                    edit : <attribute> <amount>
                    edit : <con> <str> <dex> <int> <will> <inv> <cha> <arc>
                    desc : <description>
                    delete
                    show: [user id]
                    ```""");
            event.deleteMessage();
        }
    }
}
