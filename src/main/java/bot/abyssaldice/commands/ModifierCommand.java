package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class ModifierCommand extends Command{

    public ModifierCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        String[] parameters = event.getMessageContent().split(" ");
        if(!event.getMessageAuthor().isServerAdmin() && parameters.length==9) {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", you may not!?!");
            event.deleteMessage();
            return;
        }
        if(parameters.length == 9) {
            Main.getPlayerCharacters().globalConstitution = Integer.parseInt(parameters[1]);
            Main.getPlayerCharacters().globalStrength = Integer.parseInt(parameters[2]);
            Main.getPlayerCharacters().globalDexterity = Integer.parseInt(parameters[3]);
            Main.getPlayerCharacters().globalIntelligence = Integer.parseInt(parameters[4]);
            Main.getPlayerCharacters().globalWillpower = Integer.parseInt(parameters[5]);
            Main.getPlayerCharacters().globalInvestigation = Integer.parseInt(parameters[6]);
            Main.getPlayerCharacters().globalCharisma = Integer.parseInt(parameters[7]);
            Main.getPlayerCharacters().globalArcana = Integer.parseInt(parameters[8]);
            Main.savePlayerCharacters();
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", done! ```\n" +
                    Main.getPlayerCharacters().globalConstitution + " constitution.\n" +
                    Main.getPlayerCharacters().globalStrength + " strength.\n" +
                    Main.getPlayerCharacters().globalDexterity + " dexterity.\n" +
                    Main.getPlayerCharacters().globalIntelligence + " intelligence.\n" +
                    Main.getPlayerCharacters().globalWillpower + " willpower.\n" +
                    Main.getPlayerCharacters().globalInvestigation + " investigation.\n" +
                    Main.getPlayerCharacters().globalCharisma + " charisma.\n" +
                    Main.getPlayerCharacters().globalArcana + " arcana.\n```");
        } else {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + ", globals are ```\n" +
                    Main.getPlayerCharacters().globalConstitution + " constitution.\n" +
                    Main.getPlayerCharacters().globalStrength + " strength.\n" +
                    Main.getPlayerCharacters().globalDexterity + " dexterity.\n" +
                    Main.getPlayerCharacters().globalIntelligence + " intelligence.\n" +
                    Main.getPlayerCharacters().globalWillpower + " willpower.\n" +
                    Main.getPlayerCharacters().globalInvestigation + " investigation.\n" +
                    Main.getPlayerCharacters().globalCharisma + " charisma.\n" +
                    Main.getPlayerCharacters().globalArcana + " arcana.\n```");
        }
        event.deleteMessage();
    }
}
