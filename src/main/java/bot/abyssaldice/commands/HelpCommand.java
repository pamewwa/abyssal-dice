package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class HelpCommand extends Command{

    public HelpCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        String[] parameters = event.getMessageContent().split(" ");
        if(parameters.length == 1) { //Display all commands.
            String helpMessage = "```\n";
            for(Command command : Main.getCommands()) {
                helpMessage+=Main.prefix + command.aliases.get(0) + " - " + command.info + "\n";
            }
            helpMessage += "\n```";
            event.getChannel().sendMessage(helpMessage);
            event.deleteMessage();
        }
        else {
            for(Command command : Main.getCommands()) { //Display a command's information in detail.
                for (int i2 = 0; i2 < command.aliases.size(); i2++) {
                    if (command.aliases.get(i2).equals(parameters[1])) {
                        String helpMessage = "";
                        for(int i=0; i < command.aliases.size();i++) {
                            helpMessage+="`" + Main.prefix + command.aliases.get(i) + "`, ";
                        }
                        event.getChannel().sendMessage("`" + Main.prefix + command.aliases.get(0) + " " + command.parameterGuide + " - " + command.info + "`\nAliases include: " + helpMessage + "happy to help\\❣");
                        event.deleteMessage();
                        return;
                    }
                }
            }
            event.getChannel().sendMessage("Command: `" + parameters[1] + "` is ???."); //Invalid command.
            event.deleteMessage();
        }
    }
}
