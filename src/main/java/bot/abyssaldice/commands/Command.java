package bot.abyssaldice.commands;

import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public abstract class Command implements CommandAction{
    public final List<String> aliases;
    public final String info;
    public final String parameterGuide;

    public Command(List<String> aliases, String info, String parameterGuide) {
        this.aliases = aliases;
        this.info = info;
        this.parameterGuide = parameterGuide;
    }

    public void action(MessageCreateEvent event) {
        System.out.println("A command lacks an action!");
    }
}
