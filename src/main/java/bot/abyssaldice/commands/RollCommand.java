package bot.abyssaldice.commands;

import bot.abyssaldice.Helper;
import bot.abyssaldice.Main;
import bot.abyssaldice.data_handling.Loader;
import bot.abyssaldice.data_handling.PlayerCharacter;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.Random;

public class RollCommand extends Command{

    private final static Random rand = new Random();

    public RollCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        String content = event.getMessageContent();
        if(!content.contains(" ")) {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + content + "`! How do I... this?!?");
            return;
        }
        String trim = content.substring(content.indexOf(" ")).trim();

        String modifier = Helper.acquireModifier(content); //Additional modifiers
        int attribute = acquireAttributeIfPresent(content, Main.getPlayerCharacters().players.get(event.getMessageAuthor().getId())); //Value of character attribute used, if any.

        if (attribute == -255 && content.contains("d")) { //No attribute. Basic dice roll.
            int[] diceResults = Helper.rollD(content);
            String rolls = "`";
            for(int i = 1; i < diceResults.length; i++) {
                if (i + 1 < diceResults.length) {
                    rolls += "" + (diceResults[i] + ",");
                } else {
                    rolls += (diceResults[i] + "`!");
                }
            }
            if(modifier.isBlank()) {
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() +" requested `" + trim + "`! Dice! Rolled! `[" + diceResults[0] + "]`, adding together " + rolls);
            } else {
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + trim + "`! Dice! Rolled! `[" + diceResults[0] + "]`, adding together " + rolls + " Modifier (unadded): `[" + modifier + "]`!");
            }
        } else if (attribute>-254) { //Attribute roll.
            if(trim.length()>3) {
                trim = trim.substring(0, 4);
                if(!Character.isLetterOrDigit(trim.charAt(3))) {
                    trim = trim.substring(0, 3);
                }
            } else {
                trim = trim.substring(0, 3);
            }
            if(modifier.isBlank()) {
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() +" requested `" + trim + "`! Dice! Rolled! `[" + (rand.nextInt(20)+attribute+1) + "]`, added `" + attribute + "` " + trim + "!");
            } else {
                event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + trim + "`! Dice! Rolled! `[" + (rand.nextInt(20)+attribute+1) + "]`, added `" + attribute + "` " + trim + "!" + " Modifier (unadded): `[" + modifier + "]`!");
            }
        } else {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + content + "`! How do I... this?!?");
        }
        event.deleteMessage();
    }

    private static int acquireAttributeIfPresent(String s, PlayerCharacter player) {
        String parameter = s.split(" ")[1];
        parameter+="  "; //To prevent oob errors.
        boolean paramEnd = false;
        int i = 0;
        while(!paramEnd) {
            if(Character.isLetterOrDigit(parameter.charAt(i))) {
                i++;
            } else {
                if(i>1) {
                    s = parameter.substring(0, i);
                }
                paramEnd = true;
            }
        }
        
        switch (s) {
            case "con" -> {
                return player.constitution + Main.getPlayerCharacters().globalConstitution;
            }
            case "str" -> {
                return player.strength + Main.getPlayerCharacters().globalStrength;
            }
            case "dex" -> {
                return player.dexterity + Main.getPlayerCharacters().globalDexterity;
            }
            case "int" -> {
                return player.intelligence + Main.getPlayerCharacters().globalIntelligence;
            }
            case "will" -> {
                return player.willpower + Main.getPlayerCharacters().globalWillpower;
            }
            case "inv" -> {
                return player.investigation + Main.getPlayerCharacters().globalInvestigation;
            }
            case "cha" -> {
                return player.charisma + Main.getPlayerCharacters().globalCharisma;
            }
            case "arc" -> {
                return player.arcana + Main.getPlayerCharacters().globalArcana;
            }
        }
        return -255; //Value used to indicate no attribute is present.
    }
}
