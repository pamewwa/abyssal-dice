package bot.abyssaldice.commands;

import bot.abyssaldice.Main;
import bot.abyssaldice.materials.Resource;
import bot.abyssaldice.materials.Resources;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class HarvestCommand extends Command{

    public HarvestCommand(List<String> aliases, String info, String parameterGuide) {super(aliases, info, parameterGuide);}

    @Override
    public void action(MessageCreateEvent event) {
        if(event.getMessageContent().length()>7 && event.getMessageContent().contains(" ")) {
            String[] parameters = event.getMessageContent().split(" ");
            Resource resource = Resources.getAndCloneResourceByName(parameters[1]);
            int harvests = 1; //How many resources are collected.
            int passedDC = 0; //Difficulty class passed to reduce harvest time.
            if (parameters.length > 2) {
                harvests = Integer.parseInt(parameters[2]);
            }
            if (parameters.length > 3) {
                passedDC = Integer.parseInt(parameters[3]);
            }
            if (parameters.length > 4) {
                resource.toughness = Integer.parseInt(parameters[4]);
            }
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + Main.prefix + aliases.get(0) + "`! Harvesting `" + harvests + "` `" + resource.name + "` with a toughness of `" + resource.toughness + "` for `" + resource.acquireCollectTime(harvests, passedDC) + "` `(Passed DCs: " + passedDC + ")` minutes would provide " + resource.formatHarvest(harvests) + "!");
            event.deleteMessage();
        } else {
            event.getChannel().sendMessage(event.getMessageAuthor().getDisplayName() + " requested `" + event.getMessageContent() + "`! Pam harvest what?!?");
        }
    }
}
