package bot.abyssaldice.commands;

import org.javacord.api.event.message.MessageCreateEvent;

public interface CommandAction {
    void action(MessageCreateEvent event);
}
