package bot.abyssaldice.materials;

import java.util.Map;

public class Resources {
    public final static String[] elements = {"aer","botanus","cognito","frigum","fluidus","geo","kineo","luminus","lus","metallum","machina","necros","pyro","structio","vacuus","vinculum","vitus","vol"};
    public final static Resource[] resources = {
            new Resource("null", Map.ofEntries(Map.entry("vacuus", 1.0)), 1),
            new Resource("rock", Map.ofEntries(Map.entry("geo", 1.25),Map.entry("metallum",0.3)), 0),
            new Resource("dirt", Map.ofEntries(Map.entry("geo",0.35),Map.entry("necros",0.1),Map.entry("metallum",0.05)), 3),
            new Resource("water", Map.ofEntries(Map.entry("fluidus", 1.1)), 2),
            new Resource("water_flowing", Map.ofEntries(Map.entry("fluidus",1.2),Map.entry("kineo",0.7),Map.entry("vinculum",0.3)), 2),
            new Resource("air", Map.ofEntries(Map.entry("aer",0.25),Map.entry("fluidus",0.1),Map.entry("kineo",0.05)), 1),
            new Resource("animal", Map.ofEntries(Map.entry("vitus",1.2),Map.entry("structio",0.3),Map.entry("cognito",0.3),Map.entry("machina",0.25)), 5),
            new Resource("corpse", Map.ofEntries(Map.entry("necros",1.0),Map.entry("structio",0.4),Map.entry("machina",0.2),Map.entry("vitus",0.2)), 4),
            new Resource("abyssal", Map.ofEntries(Map.entry("cognito",1.3),Map.entry("vacuus",1.1),Map.entry("necros",0.3)), 9),
            new Resource("gem_cut", Map.ofEntries(Map.entry("luminus",1.3),Map.entry("structio",1.2),Map.entry("lus",0.9)), 0),
            new Resource("gem", Map.ofEntries(Map.entry("luminus",1.1),Map.entry("structio",1.0),Map.entry("lus",0.6),Map.entry("geo",0.5)), 0),
            new Resource("gold_refined", Map.ofEntries(Map.entry("lus",1.0),Map.entry("metallum",0.6),Map.entry("geo",0.3)), 7),
            new Resource("gold", Map.ofEntries(Map.entry("lus",1.0),Map.entry("metallum",0.6),Map.entry("geo",0.5)), 6),
            new Resource("iron_refined", Map.ofEntries(Map.entry("metallum",1.0),Map.entry("geo",0.3)), 7),
            new Resource("iron", Map.ofEntries(Map.entry("metallum",0.7),Map.entry("geo",0.7)), 6),
            new Resource("steel_default", Map.ofEntries(Map.entry("metallum",1.2),Map.entry("structio",1.0)), 8),
            new Resource("ice", Map.ofEntries(Map.entry("frigum",0.75),Map.entry("structio",0.5),Map.entry("luminus",0.3)), 3),
            new Resource("snow", Map.ofEntries(Map.entry("frigum",0.5),Map.entry("fluidus",0.2)), 2),
            new Resource("slime", Map.ofEntries(Map.entry("vinculum",0.7),Map.entry("fluidus",0.6),Map.entry("vitus",0.2)), 4),
            new Resource("lava", Map.ofEntries(Map.entry("pyro",1.5),Map.entry("fluidus",0.7),Map.entry("geo",0.3),Map.entry("vinculum",0.3),Map.entry("kineo",0.2)), 3),
            new Resource("lava_flowing", Map.ofEntries(Map.entry("pyro",1.5),Map.entry("fluidus",0.8),Map.entry("geo",0.3),Map.entry("vinculum",0.5),Map.entry("kineo",0.4)), 4),
            new Resource("plant_default", Map.ofEntries(Map.entry("botanus",1.0),Map.entry("vitus",0.75),Map.entry("structio",0.35)), 1),
            new Resource("tree_default", Map.ofEntries(Map.entry("botanus",1.25),Map.entry("vitus",0.8),Map.entry("structio",0.45)), 3),
            new Resource("shroom_default", Map.ofEntries(Map.entry("botanus",0.9),Map.entry("necros",0.85),Map.entry("structio",0.4)), 1),
            new Resource("coal", Map.ofEntries(Map.entry("kineo",0.9),Map.entry("geo",0.65),Map.entry("pyro",0.6),Map.entry("necros",0.15)), 5),
            new Resource("light_dim", Map.ofEntries(Map.entry("luminus", 0.15)), 1),
            new Resource("light_bright", Map.ofEntries(Map.entry("luminus",0.2),Map.entry("kineo",0.1)), 1),
            new Resource("vim", Map.ofEntries(Map.entry("lus",1.0),Map.entry("cognito",0.3),Map.entry("structio",0.2)), 1),
            new Resource("scroll", Map.ofEntries(Map.entry("cognito",1.0),Map.entry("vol",0.5),Map.entry("structio",0.25)), 0),
            new Resource("bone_default", Map.ofEntries(Map.entry("structio",1.0),Map.entry("necros",0.6)), 5),
            new Resource("sand", Map.ofEntries(Map.entry("geo",0.5),Map.entry("luminus",0.4),Map.entry("aer",0.15)), 2),
            new Resource("quicksand", Map.ofEntries(Map.entry("fluidus",0.4),Map.entry("geo",0.4),Map.entry("vinculum",0.5)), 2),
            new Resource("adamantine_refined", Map.ofEntries(Map.entry("metallum",1.35),Map.entry("structio",1.0),Map.entry("geo",0.4),Map.entry("vol",0.4)), 11),
            new Resource("adamantine", Map.ofEntries(Map.entry("metallum",0.8),Map.entry("structio",0.7),Map.entry("geo",0.7),Map.entry("vol",0.2)), 10),
            new Resource("mithril_refined", Map.ofEntries(Map.entry("metallum",1.1),Map.entry("structio",0.9),Map.entry("aer",0.4),Map.entry("vol",0.3)), 8),
            new Resource("mithril", Map.ofEntries(Map.entry("metallum",0.8),Map.entry("structio",0.7),Map.entry("geo",0.7),Map.entry("aer",0.25),Map.entry("vol",0.1)), 7),
            new Resource("fire", Map.ofEntries(Map.entry("pyro",1.1),Map.entry("aer",0.45),Map.entry("fluidus",0.2)), 0),
            new Resource("gear", Map.ofEntries(Map.entry("machina",0.8),Map.entry("material",0.5),Map.entry("fluidus",0.3)), 3),
            new Resource("glass", Map.ofEntries(Map.entry("luminus",0.8),Map.entry("structio",0.6)), 4),
            new Resource("plastic", Map.ofEntries(Map.entry("structio",0.6),Map.entry("necros",0.3),Map.entry("fluidus",0.3),Map.entry("vitus",0.2),Map.entry("lus",0.2)), 3)
    };

    public static boolean isEther(String string) { //Identify whether a string matches the name of an ether type.
        for(String element: elements) {
            if(element.equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isResource(String string) { //Identify whether a string matches the name of a resource.
        for(Resource resource: resources) {
            if(resource.name.equalsIgnoreCase(string)) {
                return true;
            }
        }
        return false;
    }

    public static Resource getResourceByName(String string) {
        for(Resource resource: resources) {
            if(resource.name.equalsIgnoreCase(string)) {
                return resource;
            }
        }
        return resources[0];
    }

    public static Resource getAndCloneResourceByName(String string) {
        for(Resource resource: resources) {
            if(resource.name.equalsIgnoreCase(string)) {
                return new Resource(resource.name, resource.ether, resource.toughness);
            }
        }
        return resources[0];
    }

    public static String[] getResourceNames() {
        String[] list = new String[resources.length];
        for(int i = 0; i < resources.length; i++) {
            list[i] = resources[i].name;
        }
        return list;
    }
}
