package bot.abyssaldice.materials;

public class Plant {
    public final static String[] types = {"grass","leaf","bean","flower","fruit","nut","vine","bark","root","shroom","moss","other"};
    public final String type;
    public final String[] ether;

    public Plant(String type, String[] ether) {
        this.type = type;
        this.ether = ether;
    }

    public String writeEther() {
        String etherMessage = "imbued with ";
        if (ether.length<2) {
            etherMessage += "[" + ether[0] + "]!";
            return etherMessage;
        }
        for (int i = 0; i < ether.length; i++) {
            if (i + 1 < ether.length) {
                etherMessage += "[" + (ether[i] + "], ");
            } else {
                etherMessage += "and [" + (ether[i] + "]!");
            }
        }
        return etherMessage;
    }
}
