package bot.abyssaldice.materials;

import java.util.Map;

public class Resource {
    public final String name;
    public final Map<String, Double> ether; //String determines ether, integer determines amount modifier used in calculation.
    public int toughness;
    public Resource(String name, Map<String, Double> ether, int toughness) {
        this.name = name;
        this.ether = ether;
        this.toughness = toughness;
    }

    public int acquireCollectTime(int harvests, int passedDC) {
        return (toughness*(8-passedDC)*harvests)+((int)Math.pow(toughness, 2));
    }

    public String formatHarvest(int harvests) {
        var generic = new Object(){String formattedString = ""; int iteration = 0;};

        ether.forEach((key, value) -> {
            int calculatedHarvest = (int)Math.floor(value*harvests*Math.pow(toughness,1.2)/1.5);
            if (ether.size() == 1) {
                generic.formattedString += "`" + calculatedHarvest + " " + key + "`";
            } else if(generic.iteration + 1 < ether.size()) {
                generic.formattedString += "`" + calculatedHarvest + " " + key + "`, ";
                generic.iteration++;
            } else {
                generic.formattedString += "and `" + calculatedHarvest + " " + key + "`";
            }
        });
        return generic.formattedString;
    }
}
