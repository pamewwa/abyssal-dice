package bot.abyssaldice;

import java.util.Scanner;

import org.javacord.api.DiscordApi;

public final class ConsoleInterpreter implements Runnable{

    private final Scanner in = new Scanner(System.in);

    private final DiscordApi api;

    private static ConsoleInterpreter instance = null;
    private static Thread thread = null;

    public ConsoleInterpreter(DiscordApi api)
    {
        this.api = api;
    }

    public static ConsoleInterpreter getInstance(DiscordApi api)
    {
        if (instance == null)
        {
            instance = new ConsoleInterpreter(api);
            thread = new Thread(instance);
            thread.start();
        }
        return instance;
    }

    @Override
    public void run() {
        String command;
        while(true)
        {
            command = in.next();
            if (command.equalsIgnoreCase("/help")) {
                help();
            } else if (command.equalsIgnoreCase("/speak")) {
                System.out.println("Enter channel Id: ");
                speak(in.next()); //Enter channel ID
            } else if (command.equalsIgnoreCase("/vc")) { //Unused.
                System.out.println("Enter channel Id: ");
                voice(in.next()); //Enter channel ID
            }
        }
    }

    private void speak(String channelId)
    {
        System.out.println("Enter message: ");
        api.getServerTextChannelById(channelId).ifPresent(channel ->
                channel.sendMessage(in.next() + in.nextLine())); //Enter message
    }

    private void voice(String channelId) {
        api.getServerVoiceChannelById(channelId).ifPresent(channel ->
                channel.connect().thenAccept(audioConnection -> {
            // Do stuff
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        }));
    }

    private void help() {
        System.out.println("/help, /speak, /voice");
    }
}
