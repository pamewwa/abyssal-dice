package bot.abyssaldice.data_handling;

import com.google.gson.*;

import java.lang.reflect.Type;

public final class CharacterDeserializer implements JsonDeserializer<PlayerCharacter> { //Parses the json.

    @Override
    public PlayerCharacter deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject obj = jsonElement.getAsJsonObject();
        PlayerCharacter cha = new PlayerCharacter();

        cha.userId = obj.get("user_id").getAsLong();
        cha.desc = obj.get("desc").getAsString();

        cha.constitution = obj.get("constitution").getAsInt();
        cha.strength = obj.get("strength").getAsInt();
        cha.dexterity = obj.get("dexterity").getAsInt();
        cha.intelligence = obj.get("intelligence").getAsInt();
        cha.willpower = obj.get("willpower").getAsInt();
        cha.investigation = obj.get("investigation").getAsInt();
        cha.charisma = obj.get("charisma").getAsInt();
        cha.arcana = obj.get("arcana").getAsInt();

        return cha;
    }
}
