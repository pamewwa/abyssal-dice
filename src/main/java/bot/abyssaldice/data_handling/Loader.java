package bot.abyssaldice.data_handling;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public final class Loader { //Where all global modifiers and characters are stored. Also the primary class for serialization/deserialization of the json.
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting()
            .registerTypeAdapter(PlayerCharacter.class, new CharacterSerializer())
            .registerTypeAdapter(PlayerCharacter.class, new CharacterDeserializer())
            .create();

    public int globalConstitution = 0;
    public int globalStrength = 0;
    public int globalDexterity = 0;
    public int globalIntelligence = 0;
    public int globalWillpower = 0;
    public int globalInvestigation = 0;
    public int globalCharisma = 0;
    public int globalArcana = 0;
    public Map<Long, PlayerCharacter> players = new HashMap<>();

    public static Loader loadPlayerCharacters(File file) {
        Loader config;

        if (file.exists() && file.isFile()) {
            try (
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))
            ) {
                config = GSON.fromJson(bufferedReader, Loader.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to load player data.", e);
            }
        } else {
            new File("playerdata/").mkdir();
            config = new Loader();
        }
        if(config==null) {
            config = new Loader();
        }
        config.savePlayerCharacters(file);

        return config;
    }

    public void savePlayerCharacters(File config) {
        try (
                Writer writer = new OutputStreamWriter(new FileOutputStream(config), StandardCharsets.UTF_8)
        ) {
            GSON.toJson(this, writer);
        } catch (IOException e) {
            System.out.println("Failed to save player data.");
        }
    }
}
