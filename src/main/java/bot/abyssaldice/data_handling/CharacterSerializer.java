package bot.abyssaldice.data_handling;

import com.google.gson.*;

import java.lang.reflect.Type;

public final class CharacterSerializer implements JsonSerializer<PlayerCharacter>{ //Json formatting for export.

    @Override
    public JsonElement serialize(PlayerCharacter cha, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject obj = new JsonObject();
        obj.addProperty("user_id", cha.userId);
        obj.addProperty("desc", cha.desc);

        obj.addProperty("constitution", cha.constitution);
        obj.addProperty("strength", cha.strength);
        obj.addProperty("dexterity", cha.dexterity);
        obj.addProperty("intelligence", cha.intelligence);
        obj.addProperty("willpower", cha.willpower);
        obj.addProperty("investigation", cha.investigation);
        obj.addProperty("charisma", cha.charisma);
        obj.addProperty("arcana", cha.arcana);

        return obj;
    }
}