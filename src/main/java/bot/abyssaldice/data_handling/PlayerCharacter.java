package bot.abyssaldice.data_handling;

import bot.abyssaldice.Main;

public final class PlayerCharacter {
    public long userId;
    public int constitution = 0;
    public int strength = 0;
    public int dexterity = 0;
    public int intelligence = 0;
    public int willpower = 0;
    public int investigation = 0;
    public int charisma = 0;
    public int arcana = 0;

    public String desc = "";

    public PlayerCharacter() {}

    public PlayerCharacter(long userId) {
        this.userId = userId;
    }

    public PlayerCharacter(long userId, int con, int str, int dex, int inte, int will, int inv, int cha, int arc) {
        this.userId = userId;
        this.constitution = con;
        this.strength = str;
        this.dexterity = dex;
        this.intelligence = inte;
        this.willpower = will;
        this.investigation = inv;
        this.charisma = cha;
        this.arcana = arc;
    }

    public String format() {
        String formattedString;
        if (desc.isBlank()) {
            formattedString = "```\n";
        } else {
            formattedString = "```\n" + desc + "\n";
        }
        formattedString += 
        constitution + " constitution.\n" +
                strength + " strength.\n" +
                dexterity + " dexterity.\n" +
                intelligence + " intelligence.\n" +
                willpower + " willpower.\n" +
                investigation + " investigation.\n" +
                charisma + " charisma.\n" +
                arcana + " arcana.\n```";
        
        return formattedString;
    }
}
