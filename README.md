# Abyssal Dice

## About

Abyssal Dice is a Discord bot designed for a specific, homebrew tRPG system. Players may set their attributes, roll dice, and perform other game-related mechanics with ease.
To use, please create a "token" file in the main directory, and insert your bot's token into it.